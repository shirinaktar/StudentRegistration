<?php
session_start();
include_once ("../../../" . "vendor/autoload.php");

use \App\BITM\SEIP106313\Student;
use \App\BITM\SEIP106313\Message;

$student = new Student();
$std = $student->index();
$trs="";
?>


    <?php
    $sl = 0;
    foreach ($std as $student):
    $sl++;
    $trs.="<tr>";
    $trs.="<td>" .$sl."</td>";
    $trs.="<td>" .$student['title']."</td>";
    $trs.="<td>" .$student['ftitle']."</td>";
    $trs.="<td>" .$student['mtitle']."</td>";
    $trs.="<td>" .$student['birth']."</td>";
    $trs.="<td>" .$student['gender']."</td>";
    $trs.="<td>" .$student['email']."</td>";
    $trs.="<td>" .$student['mobile']."</td>";
    $trs.="<td>" .$student['address']."</td>";
    $trs.="<td>" .$student['division']."</td>";
    $trs.="<td>" .$student['course']."</td>";
    $trs.="</tr>";
    endforeach;
?>

<?php
$html = <<<AHSAN
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title>Student Registration</title>
        <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
                <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
              </head>

                    <body>
                      
                        <div class="container">
                            <table class="table table-bordered" class="table table-responsive" style="width:100%">
                                <br>  <thead>
                                        <tr>
                                            <td><b>Sl.</b></td>
                                            <td><b>Name</b></td>		
                                            <td><b>Father name</b></td>
                                            <td><b>Mother name</b></td>
                                            <td><b>Birthday</b></td>
                                            <td><b>Gender</b></td>
                                            <td><b>Email</b></td>
                                            <td><b>Mobile no.</b></td>
                                            <td><b>Address</b></td>
                                            <td><b>Division</b></td>
                                            <td><b>Course</b></td>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    echo $trs;
                                    </tbody>
                                    </table>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                                        <!-- Include all compiled plugins (below), or include individual files as needed -->
                                        <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
                                        
                                        </div>
                                        </body>
                                        </html>

AHSAN;
?>
<?php
require_once $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "StudentRegistration" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "mpdf" . DIRECTORY_SEPARATOR . "mpdf" . DIRECTORY_SEPARATOR . 'mpdf.php';
$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

